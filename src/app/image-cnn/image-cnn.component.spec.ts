import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImageCnnComponent } from './image-cnn.component';

describe('ImageCnnComponent', () => {
  let component: ImageCnnComponent;
  let fixture: ComponentFixture<ImageCnnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImageCnnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageCnnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
